/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private Player player1, player2;
    static Scanner kb = new Scanner(System.in);

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');

    }

    public void play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();

            if (table.checkWin()) {
                showWin();
                saveStat();
                showStat();
                if (checkContinue()) {
                    newGame();
                    table.switchPlayer();
                } else {
                    break;
                }
            }
            if (table.checkDraw()) {
                showDraw();
                saveDraw();
                showStat();
                if (checkContinue()) {
                    newGame();
                    table.switchPlayer();
                } else {
                    break;
                }
                
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
        System.out.println("Welcome to XO");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    public void inputRowCol() {
        System.out.print("please input row col : ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        if (table.getTable()[row - 1][col - 1] != '-') {
            System.out.println("Invalid Move! Plaese input again.");
            inputRowCol();
        } else {
            table.setRowCol(row - 1, col - 1);
        }
    }


    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void showWin() {
        showTable();
        System.out.println(table.getCurrentPlayer().getSymbol() + " is a Winner!");
    }

    private void showDraw() {
        showTable();
        System.out.println("Tie no one win");
    }
    
    private boolean checkContinue() {
        System.out.print("Continue or Exit(y/n) : ");
        String play = kb.next();
        return play.equals("y");
    }

    private void saveStat() {
        if (player1 == table.getCurrentPlayer()) {
            player1.win();
            player2.lose();
        } else {
            player1.lose();
            player2.win();
        }
    }
    
    public void saveDraw(){
        player1.draw();
        player2.draw();
    }



    private void showStat() {
        System.out.println(player1.toString());
        System.out.println(player2.toString());
    }

    

}
